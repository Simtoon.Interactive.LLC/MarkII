# MarkII

A universal digital assistant heavily inspired by Tony Stark's JARVIS.

Once it's done, it should be able to control IoT devices and interface with popular platforms that allow API access, such as Discord. Basically, MarkII should be your go-to for anything you might need during your busy workday. You'd just tell him to notify someone or let you know once something happens or start a device once a condition is met and he'd oblige.