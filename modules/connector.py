#!/home/simtoon/Documents/repos/MarkII/bin/python3

""" The module that connects the Python command interface with the SimtoonAPI backend """


import requests # we'll use this to talk to SimtoonAPI

# we'll use this method to submit calls to the SimtoonAPI. Specialized calls will have their own methods. This one is for more generalized ones
def PostToSimtoonAPI(call: str, caller: int, authorization: int):
    """
    Call SimtoonAPI and return the response

    @param call - the specific SimtoonAPI call, such as ver (returns SimtoonAPI's version)
    @param caller - the uid of the calling user
    @param authorization - API key assigned to the caller
    """
    return requests.post(f"https://auth.ongakken.com:2005/api/{call}", headers={"Content-Type": "application/json"}, json={"caller": f"{caller}","authorization:" f"{authorization}")