#!/home/simtoon/Documents/repos/MarkII/bin/python3

""" The module for MarkII's Discord interface """


import discord # the discord.py package needed to interface with Discord
from discord.ext import commands # import the commands functionality so that we can easily bind methods to Discord commands callable from there

# as per the docs, we will set our intended intents here and call the bot's constructor
intents = discord.Intents.all() # construct Discord's Intents with all available. This should grant us all intents. These need to be enabled in the Discord dev console as well
command_prefix = "!!!" # this will be the prefix for all the commands that we create
activity = discord.Activity(type=discord.ActivityType.watching, name="anime") # this appears on Discord as a status - in our case, Watching anime.
MarkII = commands.Bot(command_prefix=command_prefix, intents=intents)

## COMMANDS DEFINITION SECTION ##

# our first test command. if this runs when called, we are online and talking to the Discord bot, so no additional checks are necessary atm. Later we will do a check with SimtoonAPI
@MarkII.command()
async def ConnCheck(ctx):
    await ctx.send("Online and ready!")

#################################

## EVENT BIND SECTION ##

@MarkII.event # denotes a Discord Bot event
async def on_ready(): # once we're connected and ready to start processing
    print("Ready!") # just tell the console that we're ready


########################

# access the bot's token and run it
MarkII.run("") # the bot token from the Discord dev console. careful, this can't go into public repos, as the token is considered a secret (like passwords or API keys)